mod horaire_to_screening;
mod screening_to_horaire;
use std::io;

fn main() {
    loop {
        println!(
            "Veuillez entrer une durée au format numéraire x.xx (screening) ou en heure au format xx:xx ou avec des jours au format xx:xx:xx"
        );
        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("Échec de la lecture de l'entrée utilisateur");

        input = input.trim().to_string();

        if input.contains(",") || input.contains(".") {
            screening_to_horaire::screening_to_horaire(input);
        } else {
            horaire_to_screening::horaire_to_screening(input);
        }
    }
}
