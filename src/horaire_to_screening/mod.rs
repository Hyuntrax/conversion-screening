pub fn horaire_to_screening(input: String) {
    let input_array: Vec<&str> = input.split(':').collect();
    let minute: u32 = match input_array[input_array.len() - 1].parse::<u32>() {
        Ok(x) => x * 100 / 60,
        Err(_) => return println!("Erreur de saisie"),
    };
    let heure: u32 = match input_array[input_array.len() - 2].parse::<u32>() {
        Ok(x) => x,
        Err(_) => return println!("Erreur de saisie"),
    };
    let mut screening: f64 = (heure as f64 + minute as f64 / 100.0) / 8.0;
    if input_array.len() > 2 {
        let jour: u32 = match input_array[input_array.len() - 3].parse::<u32>() {
            Ok(x) => x,
            Err(_) => return println!("Erreur de saisie"),
        };
        screening = jour as f64 + screening as f64;
    }
    println!("le temps en screening est de : {:.2}", screening);
}
