pub fn screening_to_horaire(mut input: String) {
    input = input.replace(",", ".");
    let input: f64 = match input.parse() {
        Ok(x) => x,
        Err(_) => return println!("Erreur de saisie"),
    };

    let days = input as u32;
    let day = (input - days as f64) * 8.0;
    let heure: i32 = day as i32;
    let mut minute = day - heure as f64;
    minute = (minute * 0.6 * 100.0).round();
    if days >= 1 {
        println!("Format horaire = {}j {}h{}m", days, heure, minute);
    } else {
        println!("Format horaire = {}h{}m", heure, minute);
    }
}
